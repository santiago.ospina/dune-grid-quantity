#ifndef DUNE_GRID_QUANTITY_HH
#define DUNE_GRID_QUANTITY_HH

#include <dune/grid/quantity/entity.hh>
#include <dune/grid/quantity/entityiterator.hh>
#include <dune/grid/quantity/intersection.hh>
#include <dune/grid/quantity/intersectioniterator.hh>
#include <dune/grid/quantity/indexset.hh>
#include <dune/grid/quantity/gridview.hh>

#include <memory>
#include <vector>

namespace Dune::GridQuantity {

namespace Impl {

template<class HostGrid, class Quantity>
struct GridTraits {
  using LeafGridView = GridView<typename HostGrid::LeafGridView,Quantity>;
  using LevelGridView = GridView<typename HostGrid::LevelGridView,Quantity>;

  using LeafIntersection = QuantityIntersection<typename HostGrid::LeafIntersection,Quantity>;
  using LevelIntersection = QuantityIntersection<typename HostGrid::LevelIntersection,Quantity>;

  using LeafIntersectionIterator = QuantityIntersectionIterator<typename HostGrid::LeafIntersectionIterator,Quantity>;
  using LevelIntersectionIterator = QuantityIntersectionIterator<typename HostGrid::LevelIntersectionIterator,Quantity>;

  using LeafIndexSet = QuantityIndexSet<typename HostGrid::LeafIndexSet,Quantity>;
  using LevelIndexSet = QuantityIndexSet<typename HostGrid::LevelIndexSet,Quantity>;

  using GlobalIdSet = QuantityIdSet<typename HostGrid::GlobalIdSet,Quantity>;
  using LocalIdSet = QuantityIdSet<typename HostGrid::LocalIdSet,Quantity>;

  using HierarchicIterator = typename HostGrid::HierarchicIterator;

  using Communication = typename HostGrid::Communication;

  template<int codim>
  struct Codim {
    using Entity = QuantityEntity<typename HostGrid::template Codim<codim>::Entity, Quantity>;
    using EntitySeed = typename HostGrid::template Codim<codim>::EntitySeed;

    using Geometry = QuantityGeometry<typename HostGrid::template Codim<codim>::Geometry, Quantity>;
    using LocalGeometry = QuantityGeometry<typename HostGrid::template Codim<codim>::LocalGeometry, Quantity>;

    using LevelIterator = QuantityEntityIterator<typename HostGrid::template Codim<codim>::LevelIterator, Quantity>;
    using LeafIterator = QuantityEntityIterator<typename HostGrid::template Codim<codim>::LeafIterator, Quantity>;

    template<Dune::PartitionIteratorType partition>
    struct Partition {
      using LevelIterator = QuantityEntityIterator<typename HostGrid::template Codim<codim>::template Partition<partition>::LevelIterator, Quantity>;
      using LeafIterator = QuantityEntityIterator<typename HostGrid::template Codim<codim>::template Partition<partition>::LeafIterator, Quantity>;
    };
  };
};

}

template<Dune::Concept::Grid HostGrid, class Quantity>
class Grid : public Impl::GridTraits<HostGrid, Quantity> {
public:
  static constexpr int dimension = HostGrid::dimension;
  static constexpr int dimensionworld = HostGrid::dimensionworld;
  using ctype = Quantity;

  using Traits = Impl::GridTraits<HostGrid,Quantity>;

  Grid(const Grid&) = default;
  Grid(std::shared_ptr<HostGrid> grid_ptr) : _grid_ptr{grid_ptr} {
    updateIndexSets();
  }

  template<class EntitySeed>
  typename Traits::template Codim<EntitySeed::codimension>::Entity entity(const EntitySeed& seed) const {
    return _grid_ptr->entity(seed);
  }

  int maxLevel() const {
    return _grid_ptr->maxLevel();
  }

  int size(int level, int codim) const {
    return _grid_ptr->size(level, codim);
  }

  int size(int codim) const {
    return _grid_ptr->size(codim);
  }

  int size(Dune::GeometryType type) const {
    return _grid_ptr->size(type);
  }

  int size(int level, Dune::GeometryType type) const {
    return _grid_ptr->size(level, type);
  }

  std::size_t numBoundarySegments() const {
    return _grid_ptr->numBoundarySegments();
  }

  typename Traits::LevelGridView levelGridView(int level) const {
    return {_grid_ptr->levelGridView(level), _grid_ptr};
  }

  typename Traits::LeafGridView leafGridView() const {
    return {_grid_ptr->leafGridView(), _grid_ptr};
  }

  const typename Traits::GlobalIdSet& globalIdSet() const {
    return *_global_id_set;
  }

  const typename Traits::LocalIdSet& localIdSet() const {
    return *_local_id_set;
  }

  const typename Traits::LevelIndexSet& levelIndexSet(int level) const {
    const auto& is_ptr = _level_index_sets[level];
    assert(is_ptr);
    return *is_ptr;
  }

  const typename Traits::LeafIndexSet& leafIndexSet() const {
    return *_leaf_index_set;
  }

  bool mark(int refCount, const typename Traits::template Codim<0>::Entity& entity) {
    return _grid_ptr->mark(refCount, entity);
  }

  int getMark(const typename Traits::template Codim<0>::Entity& entity) const {
    return _grid_ptr->getMark(entity);
  }

  bool preAdapt() {
    return _grid_ptr->preAdapt();
  }

  bool adapt() {
    return _grid_ptr->adapt();
  }

  typename Traits::Communication comm() const {
    return _grid_ptr->comm();
  }

  bool loadBalance() {
    return _grid_ptr->loadBalance();
    updateIndexSets();
  }

  void globalRefine(int refCount) {
    _grid_ptr->globalRefine(refCount);
  }

  void postAdapt() {
    _grid_ptr->postAdapt();
    updateIndexSets();
  }

  void updateIndexSets() {
    _level_index_sets.resize(maxLevel());
    for(int level = 0; level != maxLevel(); ++level)
      _level_index_sets[level] = std::make_shared<typename Traits::LevelIndexSet>(_grid_ptr->levelIndexSet(level));
  }

private:
  const std::shared_ptr<HostGrid> _grid_ptr;
  const std::shared_ptr<const typename Traits::GlobalIdSet> _global_id_set;
  const std::shared_ptr<const typename Traits::LocalIdSet> _local_id_set;
  std::vector<std::shared_ptr<const typename Traits::LevelIndexSet>> _level_index_sets;
  const std::shared_ptr<const typename Traits::LeafIndexSet> _leaf_index_set;
};

} // namespace Dune::GridQuantity

#endif // DUNE_GRID_QUANTITY_HH
