#ifndef DUNE_GRID_QUANTITY_INDEX_SET_HH
#define DUNE_GRID_QUANTITY_INDEX_SET_HH

#include <dune/grid/quantity/entity.hh>

#include <dune/grid/concepts/indexset.hh>

#include <dune/geometry/type.hh>

namespace Dune::GridQuantity {

template<Dune::Concept::IndexSet HostIndexSet, class Quantity>
class QuantityIndexSet {
public:

  QuantityIndexSet(std::reference_wrapper<const HostIndexSet> is) : _is{is} {}
  QuantityIndexSet(const QuantityIndexSet&) = delete;
  QuantityIndexSet& operator=(const QuantityIndexSet&) = delete;

  template<int codim>
  struct Codim : public HostIndexSet::template Codim<codim> {
    using Entity = QuantityEntity<typename HostIndexSet::template Codim<codim>::Entity, Quantity>;
  };

  using IndexType = typename HostIndexSet::IndexType;
  using Types = typename HostIndexSet::Types;
  static constexpr int dimension = HostIndexSet::dimension;

  Types types(uint codim) const {
    return _is.get().types(codim);
  }

  IndexType size(Dune::GeometryType gt) const {
    return _is.get().size(gt);
  } 

  IndexType size(uint codim) const {
    return _is.get().size(codim);
  }

  template<int codim>
  IndexType index(const typename Codim<codim>::Entity& entity) const {
    return _is.get().index(entity.hostEntity());
  }

  template<class Entity>
  IndexType index(const Entity& entity) const {
    return _is.get().index(entity.hostEntity());
  }

  template<int codim>
  IndexType subIndex(const typename Codim<codim>::Entity& entity, int i, uint sub_codim) const {
    return _is.get().subIndex(entity.hostEntity(), i, sub_codim);
  }

  template<class Entity>
  IndexType subIndex(const Entity& entity, int i, uint sub_codim) const {
    return _is.get().subIndex(entity.hostEntity(), i, sub_codim);
  }

  template<class Entity>
  bool contains(const Entity& entity) const {
    return _is.get().contains(entity.hostEntity());
  }

  const HostIndexSet& hostIndexSet() const {
    return _is.get();
  }

private:
  std::reference_wrapper<const HostIndexSet> _is;
};


template<Dune::Concept::IdSet HostIdSet, class Quantity>
class QuantityIdSet {
public:

  QuantityIdSet(std::reference_wrapper<const HostIdSet> is) : _is{is} {}
  QuantityIdSet(const QuantityIdSet&) = delete;
  QuantityIdSet& operator=(const QuantityIdSet&) = delete;

  template<int codim>
  struct Codim : public HostIdSet::template Codim<codim> {
    using Entity = QuantityEntity<typename HostIdSet::template Codim<codim>::Entity, Quantity>;
  };

  using IdType = typename HostIdSet::IdType;
  static constexpr int dimension = HostIdSet::dimension;

  template<int codim>
  IdType id(const typename Codim<codim>::Entity& entity) const {
    return _is.get().id(entity.hostEntity());
  }

  template<class Entity>
  IdType id(const Entity& entity) const {
    return _is.get().id(entity.hostEntity());
  }

  IdType subId(const typename Codim<0>::Entity& entity, int i, uint codim) const {
    return _is.get().subId(entity.hostEntity(), i, codim);
  }

  const HostIdSet& hostIdSet() const {
    return _is.get();
  }

private:
  const std::reference_wrapper<const HostIdSet> _is;
};

} // namespace Dune::GridQuantity

#endif // DUNE_GRID_QUANTITY_INDEX_SET_HH
