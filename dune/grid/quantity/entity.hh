#ifndef DUNE_GRID_QUANTITY_ENTITY_HH
#define DUNE_GRID_QUANTITY_ENTITY_HH

#include <dune/grid/quantity/geometry.hh>

#include <dune/grid/concepts/entity.hh>

namespace Dune::GridQuantity {

template<Dune::Concept::Entity HostEntity, class Quantity>
class QuantityEntity : public HostEntity {
public:
  template<int c>
  struct Codim : public HostEntity::template Codim<c> {
    using Entity = QuantityEntity<typename HostEntity::template Codim<c>::Entity, Quantity>;
  };

  using Geometry = QuantityGeometry<typename HostEntity::Geometry,Quantity>;

  QuantityEntity() {}
  QuantityEntity(const HostEntity& entity) : HostEntity{entity} {}
  QuantityEntity(HostEntity&& entity) : HostEntity{std::move(entity)} {}

  Geometry geometry() const {
    return Geometry{HostEntity::geometry()};
  }

  const HostEntity& hostEntity() const {
    return *this;
  }

  HostEntity& hostEntity() {
    return *this;
  }
};

} // namespace Dune::GridQuantity

#endif // DUNE_GRID_QUANTITY_ENTITY_HH
