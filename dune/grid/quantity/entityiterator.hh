#ifndef DUNE_GRID_QUANTITY_ENTITY_ITERATOR_HH
#define DUNE_GRID_QUANTITY_ENTITY_ITERATOR_HH

#include <dune/grid/quantity/entity.hh>

#include <dune/grid/concepts/entityiterator.hh>

namespace Dune::GridQuantity {

template<Dune::Concept::EntityIterator HostEntityIterator, class Quantity>
class QuantityEntityIterator : public HostEntityIterator {
public:

  QuantityEntityIterator() {}
  QuantityEntityIterator(const HostEntityIterator& it) : HostEntityIterator{it} {}
  QuantityEntityIterator(HostEntityIterator&& it) : HostEntityIterator{std::move(it)} {}

  using Entity = QuantityEntity<typename HostEntityIterator::Entity, Quantity>;

  Entity operator*() const {
    return Entity{*static_cast<const HostEntityIterator&>(*this)};
  }

  Entity operator*() {
    return Entity{*static_cast<HostEntityIterator&>(*this)};
  }

  QuantityEntityIterator &operator++ () {
    ++static_cast<HostEntityIterator&>(*this);
    return *this; 
  }

  QuantityEntityIterator operator++ (int) {
    return static_cast<HostEntityIterator&>(*this)++;
  }

  Entity operator->() {
    return *(*this);
  }

  Entity operator->() const {
    return *(*this);
  }
};


} // namespace Dune::GridQuantity

namespace std
{
  template<class HostEntityIterator, class Quantity>
  struct iterator_traits<Dune::GridQuantity::QuantityEntityIterator<HostEntityIterator,Quantity>>
  {
    typedef ptrdiff_t difference_type;
    typedef const typename Dune::GridQuantity::QuantityEntityIterator<HostEntityIterator,Quantity>::Entity value_type;
    typedef value_type *pointer;
    typedef value_type &reference;
    typedef forward_iterator_tag iterator_category;
  };

} // namespace std

#endif // DUNE_GRID_QUANTITY_ENTITY_ITERATOR_HH
