#ifndef DUNE_GRID_QUANTITY_GRID_VIEW_HH
#define DUNE_GRID_QUANTITY_GRID_VIEW_HH

#include <dune/grid/quantity/entity.hh>
#include <dune/grid/quantity/entityiterator.hh>
#include <dune/grid/quantity/intersection.hh>
#include <dune/grid/quantity/intersectioniterator.hh>
#include <dune/grid/quantity/indexset.hh>

#include <dune/grid/concepts/grid.hh>

#include <memory>

namespace Dune::GridQuantity {

template<Dune::Concept::Grid Grid, class Quantity>
class QuantityGrid;

template<Dune::Concept::GridView HostGridView, class Quantity>
class QuantityGridView : public HostGridView {
public:

  using ctype = Quantity;
  using IndexSet = QuantityIndexSet<typename HostGridView::IndexSet, Quantity>;
  using IntersectionIterator = QuantityIntersectionIterator<typename HostGridView::IntersectionIterator, Quantity>;
  using Grid = QuantityGrid<typename HostGridView::Grid, Quantity>;

  template<int codim>
  struct Codim : public HostGridView::template Codim<codim> {

    using Entity = QuantityEntity<typename HostGridView::template Codim<codim>::Entity, Quantity>;
    using Geometry = QuantityGeometry<typename HostGridView::template Codim<codim>::Geometry, Quantity>;
    using LocalGeometry = QuantityGeometry<typename HostGridView::template Codim<codim>::LocalGeometry, Quantity>;
    using Iterator = QuantityEntityIterator<typename HostGridView::template Codim<codim>::Iterator, Quantity>;

    template<Dune::PartitionIteratorType partition>
    struct Partition : public HostGridView::template Codim<codim>::template Partition<partition> {
      using Iterator = QuantityEntityIterator<typename HostGridView::template Codim<codim>::template Partition<partition>::Iterator, Quantity>;
    };
  };

  QuantityGridView(const QuantityGridView&) = default;
  QuantityGridView(const HostGridView& grid_view,
                    std::shared_ptr<const typename HostGridView::Grid> grid_ptr)
    : HostGridView{grid_view}
    , _grid_ptr{std::move(grid_ptr)}
    , _is{std::make_shared<IndexSet>(grid_view.indexSet())}
  {}

  template<int codim>
  typename Codim<codim>::Iterator begin() const {
    return HostGridView::template begin<codim>();
  }

  template<int codim>
  typename Codim<codim>::Iterator end() const {
    return HostGridView::template end<codim>();
  }


  template<int codim, Dune::PartitionIteratorType partition>
  typename Codim<codim>::template Partition<partition>::Iterator begin() const
  {
    return HostGridView::template begin<codim,partition>();
  }

  template<int codim, Dune::PartitionIteratorType partition>
  typename Codim<codim>::template Partition<partition>::Iterator end() const
  {
    return HostGridView::template end<codim,partition>();
  }


  IntersectionIterator ibegin(const typename Codim<0>::Entity& entity) const {
    return HostGridView::ibegin(entity);
  }

  IntersectionIterator iend(const typename Codim<0>::Entity& entity) const {
    return HostGridView::iend(entity);
  }

  template<class Entity>
  bool contains(const Entity& entity) const {
    return HostGridView::contains(entity.hostEntity());
  }

  const IndexSet& indexSet() const {
    return *_is;
  }

  const Grid& grid() const {
    return Grid{*_grid_ptr};
  }

private:
  std::shared_ptr<const typename HostGridView::Grid> _grid_ptr;
  std::shared_ptr<const IndexSet> _is;
};

} // namespace Dune::GridQuantity

#endif // DUNE_GRID_QUANTITY_GRID_VIEW_HH
