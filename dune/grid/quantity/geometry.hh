#ifndef DUNE_GRID_QUANTITY_GEOMETRY_HH
#define DUNE_GRID_QUANTITY_GEOMETRY_HH

#include <dune/common/fvector.hh>

#include <dune/grid/concepts/geometry.hh>

#include <bit>

namespace Dune::GridQuantity {

template<Dune::Concept::Geometry HostGeometry, class Quantity>
class QuantityGeometry : public HostGeometry {

  template<std::size_t exp = HostGeometry::mydimension>
  static constexpr auto implQuantityPower(auto t) {
    if constexpr (exp == 0)
      return t;
    else
      return t*implQuantityPower<exp-1>(t);
  }

public:

  using ctype = Quantity;
  using typename HostGeometry::LocalCoordinate;
  using GlobalCoordinate = Dune::FieldVector<ctype,HostGeometry::mydimension>;
  using Volume = decltype(implQuantityPower(Quantity{}));

  QuantityGeometry(const HostGeometry& geo) : HostGeometry{geo} {}

  GlobalCoordinate corner(int i) const {
    return std::bit_cast<GlobalCoordinate>(HostGeometry::corner(i));
  }

  GlobalCoordinate global(LocalCoordinate local) const {
    return std::bit_cast<GlobalCoordinate>(HostGeometry::global(local));
  }

  LocalCoordinate local(GlobalCoordinate global) const {
    return HostGeometry::local(std::bit_cast<typename HostGeometry::GlobalCoordinate>(global));
  }

  Volume volume() const {
    return std::bit_cast<Volume>(HostGeometry::volume());
  }

  GlobalCoordinate center() const {
    return std::bit_cast<Volume>(HostGeometry::center());
  }

  Volume integrationElement(const LocalCoordinate& local) const
  {
    return std::bit_cast<Volume>(HostGeometry::integrationElement(local));
  }

  friend decltype(auto) referenceElement(const QuantityGeometry& geo)
  {
    return referenceElement(static_cast<const HostGeometry&>(geo));
  }

};

} // namespace Dune::GridQuantity

#endif // DUNE_GRID_QUANTITY_GEOMETRY_HH
