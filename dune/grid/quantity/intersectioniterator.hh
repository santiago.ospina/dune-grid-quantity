#ifndef DUNE_GRID_QUANTITY_ENTITY_INTERSECTION_HH
#define DUNE_GRID_QUANTITY_ENTITY_INTERSECTION_HH

#include <dune/grid/quantity/intersection.hh>

#include <dune/grid/concepts/intersectioniterator.hh>

namespace Dune::GridQuantity {

template<Dune::Concept::IntersectionIterator HostIntersectionIterator, class Quantity>
class QuantityIntersectionIterator : public HostIntersectionIterator {
public:

  QuantityIntersectionIterator() {}
  QuantityIntersectionIterator(const HostIntersectionIterator& it) : HostIntersectionIterator{it} {}
  QuantityIntersectionIterator(HostIntersectionIterator&& it) : HostIntersectionIterator{std::move(it)} {}

  using Intersection = QuantityIntersection<typename HostIntersectionIterator::Intersection, Quantity>;

  Intersection operator*() const {
    return Intersection{*static_cast<const HostIntersectionIterator&>(*this)};
  }

  Intersection operator*() {
    return Intersection{*static_cast<HostIntersectionIterator&>(*this)};
  }

  QuantityIntersectionIterator &operator++ () {
    ++static_cast<HostIntersectionIterator&>(*this);
    return *this; 
  }

  QuantityIntersectionIterator operator++ (int) {
    return static_cast<HostIntersectionIterator&>(*this)++;
  }

  Intersection operator->() {
    return *(*this);
  }

  Intersection operator->() const {
    return *(*this);
  }
};

} // namespace Dune::GridQuantity

namespace std
{
  template<class HostIntersectionIterator, class Quantity>
  struct iterator_traits<Dune::GridQuantity::QuantityIntersectionIterator<HostIntersectionIterator,Quantity>>
  {
    typedef ptrdiff_t difference_type;
    typedef const typename Dune::GridQuantity::QuantityIntersectionIterator<HostIntersectionIterator,Quantity>::Intersection value_type;
    typedef value_type *pointer;
    typedef value_type &reference;
    typedef forward_iterator_tag iterator_category;
  };

} // namespace std

#endif // DUNE_GRID_QUANTITY_ENTITY_INTERSECTION_HH

