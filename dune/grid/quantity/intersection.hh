#ifndef DUNE_GRID_QUANTITY_INTERSECTION_HH
#define DUNE_GRID_QUANTITY_INTERSECTION_HH

#include <dune/grid/quantity/entity.hh>
#include <dune/grid/quantity/geometry.hh>

#include <dune/grid/concepts/intersection.hh>

#include <bit>

namespace Dune::GridQuantity {

template<Dune::Concept::Intersection HostIntersection, class Quantity>
class QuantityIntersection : public HostIntersection {
public:
  using Entity = QuantityEntity<typename HostIntersection::Entity, Quantity>;
  using Geometry = QuantityGeometry<typename HostIntersection::Geometry,Quantity>;
  using LocalGeometry = QuantityGeometry<typename HostIntersection::LocalGeometry,Quantity>;
  using GlobalCoordinate = typename Geometry::GlobalCoordinate;
  using LocalCoordinate = typename Geometry::LocalCoordinate;
  using ctype = typename Geometry::ctype;

  QuantityIntersection() {}
  QuantityIntersection(const HostIntersection& intersection) : HostIntersection{intersection} {}
  QuantityIntersection(HostIntersection&& intersection) : HostIntersection{std::move(intersection)} {}

  Entity inside() const {
    return Entity{HostIntersection::inside()};
  }

  Entity outside() const {
    return Entity{HostIntersection::outside()};
  }

  LocalGeometry geometryInInside() const {
    return LocalGeometry{HostIntersection::geometryInInside()};
  }

  LocalGeometry geometryInOutside() const {
    return LocalGeometry{HostIntersection::geometryInOutside()};
  }

  GlobalCoordinate outerNormal(LocalCoordinate local) const {
    return std::bit_cast<GlobalCoordinate>(HostIntersection::outerNormal(local));
  }

  GlobalCoordinate integrationOuterNormal(LocalCoordinate local) const {
    return std::bit_cast<GlobalCoordinate>(HostIntersection::integrationOuterNormal(local));
  }

  GlobalCoordinate unitOuterNormal(LocalCoordinate local) const {
    return std::bit_cast<GlobalCoordinate>(HostIntersection::unitOuterNormal(local));
  }

GlobalCoordinate centerUnitOuterNormal() const {
    return std::bit_cast<GlobalCoordinate>(HostIntersection::centerUnitOuterNormal());
  }
};

} // namespace Dune::GridQuantity

#endif // DUNE_GRID_QUANTITY_INTERSECTION_HH
